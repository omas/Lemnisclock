//  LemnisClock - The Lemniscatic Clock
//  Copyright (C) 2001-2011  Orestes Mas
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <QtGui>

#include "mainwindow.h"
#include "lemniswidget.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setMinimumSize(QSize(640,480));
    createActions();
    createMenus();
    lw = new LemnisWidget(this);
    setCentralWidget(lw);
    sb = new QStatusBar(this);
    setStatusBar(sb);
    
    setWindowTitle("The Lemniscatic Clock");

    secondsTimer = new QTimer(this);
    connect(secondsTimer, SIGNAL(timeout()), this, SLOT(updateTime()));
    secondsTimer->start(1000);    
}

MainWindow::~MainWindow()
{
    secondsTimer->stop();
    delete secondsTimer;
}

void MainWindow::displayMessage(QString msg)
{
    sb->showMessage(msg);
}

void MainWindow::updateTime()
{
  displayMessage(QTime::currentTime().toString("hh:mm:ss"));
  lw->update();
}

void MainWindow::createActions()
{
    exitAction = new QAction(tr("E&xit"), this);
    exitAction->setShortcut(tr("Ctrl+Q"));
    exitAction->setStatusTip(tr("Exit the application"));
    connect(exitAction, SIGNAL(triggered()), this, SLOT(close()));
}

void MainWindow::createMenus()
{
  fileMenu = menuBar()->addMenu(tr("&File"));
  fileMenu->addAction(exitAction);
}
