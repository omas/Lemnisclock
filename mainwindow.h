//  LemnisClock - The Lemniscatic Clock
//  Copyright (C) 2001-2011  Orestes Mas
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>

class LemnisWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
    
private slots:
    void displayMessage(QString msg);
    void updateTime();

private:
    LemnisWidget *lw;
    QMenu *fileMenu;
    QAction *exitAction;
    QStatusBar *sb;
    QTimer *secondsTimer;
    void createActions();
    void createMenus();    
};

#endif // MAINWINDOW_H
