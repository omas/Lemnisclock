//  LemnisClock - The Lemniscatic Clock
//  Copyright (C) 2001-2011  Orestes Mas
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LEMNISPOINTS_H
#define LEMNISPOINTS_H

#include <QtCore>

class lemnisPoints
{
public:
    // M array holds the 60 equally-spaced points over the lemniscate
    static const QPointF M[60];

    // A and B arrays hold 60 points over the left and right circles, that are
    // part of the lemniscate-generating arms (the other two are the two centers
    // of mentioned circles)
    static const QPointF A[60], B[60];

    // grad array holds the normalized directing vectors of the line perpendicular
    // (except at origin) to the lemniscate at every M-point.
    static const QPointF grad[60];

    // These points hold the centers of the two generating circles
    static const QPointF centerA, centerB;
};

#endif // LEMNISPOINTS_H
