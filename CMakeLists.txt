project(lemnisclock)
cmake_minimum_required(VERSION 2.8.5)

cmake_policy(SET CMP0007 NEW)

find_package(Qt4 REQUIRED)

include(${QT_USE_FILE})

include_directories( 
  ${QT_INCLUDE_DIR} 
  ${CMAKE_CURRENT_BINARY_DIR} 
  ${CMAKE_CURRENT_SOURCE_DIR}
)

set(CMAKE_BUILD_TYPE "Debug")

set(lemnisclock_UIS
)

set(lemnisclock_MOC_FONTS
  lemniswidget.h
  mainwindow.h
)

set(lemnisclock_FONTS
  lemnispoints.cpp
  lemniswidget.cpp
  mainwindow.cpp
  main.cpp
)

qt4_wrap_ui(lemnisclock_UIS_H ${lemnisclock_UIS})
qt4_wrap_cpp(lemnisclock_MOC_UI ${lemnisclock_UIS_H})
qt4_wrap_cpp(lemnisclock_MOCS ${lemnisclock_MOC_FONTS})

add_executable(lemnisclock
  ${lemnisclock_FONTS}
  ${lemnisclock_MOCS}
  ${lemnisclock_MOC_UI}
)

target_link_libraries(lemnisclock ${QT_LIBRARIES} pthread)

install(TARGETS lemnisclock DESTINATION bin)

# uninstall target
configure_file(
    "${CMAKE_CURRENT_SOURCE_DIR}/cmake_uninstall.cmake.in"
    "${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake"
    IMMEDIATE @ONLY)

add_custom_target(uninstall
    COMMAND ${CMAKE_COMMAND} -P ${CMAKE_CURRENT_BINARY_DIR}/cmake_uninstall.cmake)

SET(CPACK_GENERATOR "DEB")
SET(CPACK_PACKAGE_NAME ${CMAKE_PROJECT_NAME})
SET(CPACK_SET_DESTDIR TRUE)
SET(CPACK_DEBIAN_PACKAGE_MAINTAINER "Orestes Mas")
SET(CPACK_PACKAGE_VERSION_MAJOR "0")
SET(CPACK_PACKAGE_VERSION_MINOR "0")
SET(CPACK_PACKAGE_VERSION_PATCH "1~alpha1")
include(CPack)

