//  LemnisClock - The Lemniscatic Clock
//  Copyright (C) 2001-2011  Orestes Mas
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef LEMNISWIDGET_H
#define LEMNISWIDGET_H

#include <QtGui/QWidget>
#include <QtGui/QPainter>
#include <QTime>

class LemnisWidget : public QWidget
{
    Q_OBJECT

  public:
    LemnisWidget(QWidget *parent = 0);
  
  protected:
    void paintEvent(QPaintEvent *event);
    void resizeEvent(QResizeEvent * event);

  private:
    QPainter painter;
    QTransform tx, invtx;
    QPainterPath circleA, circleB;
    QPainterPath lemnisPath;
    QVector<QPointF> numberPositions;
    QVector<QPainterPath> numberPaths;
    bool sizeChanged;
    void paintBackground();
    void paintArms(QTime t);
    void recalcTransform();
};

#endif // LEMNISWIDGET_H
