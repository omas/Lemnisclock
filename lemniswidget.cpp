//  LemnisClock - The Lemniscatic Clock
//  Copyright (C) 2001-2011  Orestes Mas
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.

#include <math.h>
#include <QPainter>
#include <QtGlobal>
#include <QTime>

#include "lemniswidget.h"
#include "lemnispoints.h"

LemnisWidget::LemnisWidget(QWidget *parent)
    : QWidget(parent)
{
    // On creation, schedule a complete Background repaint
    sizeChanged = true;
    
    //Calculate the generating circles path to be drawn later
    circleA.addEllipse(lemnisPoints::centerA, 1.0, 1.0);
    circleB.addEllipse(lemnisPoints::centerB, 1.0, 1.0);

    //Calculate the lemniscata path to be drawn later
    int maxPoints = 90;
    qreal r;
    lemnisPath.moveTo(0.0, 0.0);
    qreal theta = 0.0;
    for (int i = 1; i<maxPoints; i++)
    {
      theta = (-M_PI/4 + M_PI/2*i/maxPoints);
      r = cos(2*theta);
      if (r >= 0) r = sqrt(r);
      lemnisPath.lineTo(r*cos(theta), r*sin(theta));
    }
    lemnisPath.lineTo(0.0, 0.0);
    for (int i = 1; i<maxPoints; i++)
    {
      theta = (3*M_PI/4 + M_PI/2*i/maxPoints);
      r = cos(2*theta);
      if (r >= 0) r = sqrt(r);
      lemnisPath.lineTo(r*cos(theta), r*sin(theta));
    }
    lemnisPath.lineTo(0.0, 0.0);
    
    //Now calculate tick marks, and hour labels
    qreal offset = 0.0;
    qreal lambda = 0.0;
    numberPositions.reserve(12);
    numberPaths.reserve(12);
    QFont f("helvetica");
    f.setStyleStrategy(QFont::ForceOutline);
    f.setPointSize(48);
    f.setStyleHint(QFont::Helvetica);
    QFontMetrics fm(f);
    for (int i=0 ; i<60 ; i++)
    {
      if (i % 5 == 0) {
        if (i % 30 == 0) {
          // Parameters for largest ticks
          offset = 0.05;
          lambda = 0.12;
          // Calculate label position in this case, and save into a vector
          QPointF insertionPoint(
            lemnisPoints::M[i].x() + (offset+lambda+0.08)*lemnisPoints::grad[i].x(),
            lemnisPoints::M[i].y() + (offset+lambda+0.08)*lemnisPoints::grad[i].y()
          );
          numberPositions << insertionPoint;
        }
        else {  //larger tick, every hour
          offset = 0.0;
          lambda = -0.06;
          // Calculate label position in this case, and save into a vector
          QPointF insertionPoint(
            lemnisPoints::M[i].x() + (offset+lambda-0.05)*lemnisPoints::grad[i].x(),
            lemnisPoints::M[i].y() + (offset+lambda-0.05)*lemnisPoints::grad[i].y()
          );
          numberPositions << insertionPoint;
        } // if (i % 30)
        QPainterPath hourNumber;
        hourNumber.addText(QPointF(0.0,0.0), f, QString::number( (i==0 ? 12 : i/5 )));
        numberPaths << hourNumber;
      }
      else  //normal tick, w/o number
      {
        offset = 0.0;
        lambda = -0.03;
      } // if (i % 5)
      lemnisPath.moveTo(lemnisPoints::M[i].x() + offset*lemnisPoints::grad[i].x(),
                        lemnisPoints::M[i].y() + offset*lemnisPoints::grad[i].y());
      lemnisPath.lineTo(lemnisPoints::M[i].x() + (offset+lambda)*lemnisPoints::grad[i].x(),
                        lemnisPoints::M[i].y() + (offset+lambda)*lemnisPoints::grad[i].y());
    }
    
    //Finally generate a vector with each hour label's position

    for (int i=0; i<numberPaths.count(); ++i)
    {
      //FIXME: Must calculate the scale factor in a more 'scientifc' way
      // Scale font outlines to something that fits into our coordinate system
      numberPaths[i] = numberPaths[i] * QMatrix(0.001, 0, 0, -0.001, 0, 0);
      // Translate each label to the precalculated position.
      qreal dx = numberPositions[i].x() - numberPaths[i].boundingRect().width()/2;
      qreal dy = numberPositions[i].y() - numberPaths[i].boundingRect().height()/2;
      numberPaths[i] = numberPaths[i] * QMatrix().translate(dx,dy);
    }
}

void LemnisWidget::paintEvent(QPaintEvent *event)
{
  //Rationale (TO BE DONE YET):
  // We can maintain a QPixmap with a prepainted clock background
  // This should be reusable as long as widget size remains unchanged
  // If so, simply paint the pixmap
  // If not, recalculate background first
    if (sizeChanged)
    {
      //recalcBackground();
      sizeChanged = false;
    }
    paintBackground();
    paintArms(QTime::currentTime());
}

void LemnisWidget::resizeEvent(QResizeEvent *event)
{
  sizeChanged = true;
  update();
}

void LemnisWidget::paintBackground()
{
    painter.begin(this);
    painter.setRenderHint(QPainter::Antialiasing);
    recalcTransform();
    painter.setWorldTransform(tx);

    //Now paint the two generating circles
    QPen circlePen = QPen();
    //FIXME: Parametrize this color (?)
    circlePen.setColor(Qt::lightGray);
    //FIXME: Parametrize this thickness (?)
    circlePen.setWidthF(0.01);
    painter.setPen(circlePen);
    painter.drawPath(circleA);
    painter.drawPath(circleB);

    //Paint the Lemniscata (QPath lemnisPath)
    QPen lemnisPen = QPen();
    //FIXME: Parametrize this color (?)
    lemnisPen.setColor(Qt::blue);
    //FIXME: Parametrize this thickness (?)
    lemnisPen.setWidthF(0.01);
    painter.setPen(lemnisPen);
    painter.drawPath(lemnisPath);
    
    //Paint the Hours numbers (QPath numbersPath)
    QPen numbersPen = QPen();
    //FIXME: Parametrize this color (?)
    lemnisPen.setColor(Qt::black);
    //FIXME: Parametrize this thickness (?)
    //numbersPen.setWidthF(2);
    painter.setPen(numbersPen);
    painter.setBrush(Qt::SolidPattern);

    for (int i=0; i<numberPaths.count(); ++i)
      painter.drawPath(numberPaths[i]);

    painter.end();
}

void LemnisWidget::paintArms(QTime t)
{
    painter.begin(this);
    recalcTransform();
    painter.setWorldTransform(tx);
    painter.setRenderHint(QPainter::Antialiasing);

    int secs = t.second();

    // Draw hour mark
    //FIXME: Parametrize this color (?)
    painter.setPen(Qt::yellow);
    painter.setBrush(Qt::yellow);
    painter.drawEllipse(lemnisPoints::M[(t.hour() % 12)*5], 0.02, 0.02);

    // Draw minute mark
    //FIXME: Parametrize this color (?)
    painter.setPen(Qt::green);
    painter.setBrush(Qt::green);
    painter.drawEllipse(lemnisPoints::M[t.minute()], 0.02, 0.02);

    // Draw seconds mark
    //FIXME: Parametrize this color (?)
    painter.setPen(Qt::red);
    painter.setBrush(Qt::red);
    painter.drawEllipse(lemnisPoints::M[t.second()], 0.02, 0.02);

    // Draw arms of the lemniscata's geometrical construct
    QPen armsPen = QPen();
    armsPen.setWidthF(0.02);
    //FIXME: Parametrize this color (?)
    armsPen.setColor(Qt::red);
    armsPen.setCapStyle(Qt::RoundCap);
    armsPen.setJoinStyle(Qt::RoundJoin);
    painter.setPen(armsPen);
    QPainterPath arms(lemnisPoints::centerA);
    arms.lineTo(lemnisPoints::A[secs]);
    arms.lineTo(lemnisPoints::B[secs]);
    arms.lineTo(lemnisPoints::centerB);
//    painter.setBrush(Qt::NoBrush);
    painter.strokePath(arms, armsPen);

    painter.end();
}

void LemnisWidget::recalcTransform()
{
    tx.reset();
   /*
       Let's calculate a transform to convert window coordinates into a cartesian graph, with Y increasing upwards
       and with origin at the window's center.
       We also scale the coordinates to allow working with a normalized values and to easily autoscale graph upon
       window resizing.
    */
    tx.translate(width()/2, height()/2);
    /*
       Calculate scaling factor. We need a normalized window of 2+sqrt(2) by 2, so the scaling factor
       is the real width and height divided by either factor (slightly reduced to get some guard space near 
       widget's borders)
    */
    qreal reductionFactor = 1.05;
    qreal scaleFactor = qMin(width()/(2.0 + sqrt(2.0))/reductionFactor, height()/2.0/reductionFactor);
    tx.scale(scaleFactor,-scaleFactor);
    /*
       Calculate the inverse transform, necessary to be able to paint text
    */
    invtx = tx.inverted();
}
